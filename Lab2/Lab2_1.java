package Lab2;

/**
 * Created by barmotina on 12-Apr-16.
 */
//1. Напишите примеры использования данных операторов и выведите результаты на экран
public class Lab2_1 {
    public static void main(String[] args) {
        int a = 5;
        int b = 2;
        //Операции Арифметические
        System.out.println(a + b);
        //Операции сравнения
        System.out.println(a == b);
        //Операция присвоения
        int x = 2;
        x += 4; // x=6

        //2. Переведите значение каждого примитивного типа в строку и обратно - выведите результаты на  экран
        byte b1 = 1;
        int i1 = 2;
        short s1 = 32456;
        char c1 = 'a';
        long l1 = 20L;
        float f1 = 12.34f;
        double d1 = 23.45d;
        boolean bool1 = true;

        String b2 = b1 + "5";
        System.out.println(b2);
        String i2 = Integer.toString(i1);
        System.out.println(i2);
        String s2 = Short.toString(s1);
        System.out.println(s2);
        String c2 = Character.toString(c1);
        System.out.println(c2);
        String l2 = Long.toString(l1);
        System.out.println(l2);
        String f2 = Float.toString(f1);
        System.out.println(f2);
        String d2 = Double.toString(d1);
        System.out.println(d2);
        String bool2 = Boolean.toString(bool1);
        System.out.println(bool2);


        byte b3 = (Byte.valueOf(b2)).byteValue();
        System.out.println(b3);
        int i3 = (Integer.valueOf(i2)).intValue();
        System.out.println(i3);
        short s3 = (Short.valueOf(i2)).shortValue();
        System.out.println(s3);
        //char c3 = (Character.valueOf(c2)).charValue();
        //System.out.println(c3);
        char c3 = c2.charAt(0);
        System.out.println(c3);
        long l3 = (Long.valueOf(l2)).longValue();
        System.out.println(l3);
        float f3 = (Float.valueOf(f2)).floatValue();
        System.out.println(f3);
        double d3 = (Double.valueOf(f2)).doubleValue();
        System.out.println(d3);
        boolean bool3 = (Boolean.valueOf(bool2)).booleanValue();
        System.out.println(bool3);

        // 3. Переведите число с точкой в тип без точки и наоборот - выведите результаты

        double d5 = (double) (i1);
        System.out.println(d5);

        int i4 = (int) d5;
        System.out.println(i4);

    }
}
