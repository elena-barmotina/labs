package Lab2;

/**
 * Created by barmotina on 13-Apr-16.
 */

//Написать программу которая бы запросила пользователя ввести число, затем знак математической операции, затем еще число и вывела результат. Должно работать и для целых и для дробных чисел

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Lab2_4 {
    public static void main (String[] args) throws Exception {

        BufferedReader buffread =  new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter the first number and click Enter key: ");
        String a1 = buffread.readLine();
        System.out.println("Please enter mathematical operation and click Enter key: ");
        String operation=buffread.readLine();
        System.out.println("Please enter the second number and click Enter key: ");
        String b1 = buffread.readLine();

        float a=Float.parseFloat(a1);
        char oper=operation.charAt(0);
        float b=Float.parseFloat(b1);

        if (oper=='+')
        {
            System.out.println("Result= "+(a+b));
        }
        else if (oper=='-')
        {
            System.out.println("Result= "+(a-b));
        }
        else if (oper=='*')
        {
            System.out.println("Result= "+(a*b));
        }
        else if (oper=='/')
        {
            System.out.println("Result= "+(a/b));
        }
        else {
            System.out.println("Incorrect mathematical operation");
        }

    }
}
