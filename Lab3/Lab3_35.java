package Lab3;

/**
 * Created by barmotina on 23-May-16.
 */
//3. Вывести все простые числа от 1 до N. N задается пользователем через консоль
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Lab3_35 {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number: ");
        String str = reader.readLine();
        int m = Integer.parseInt(str);
        if(m <= 0)
        {
            System.out.println("Number must be positive");
        }
        else
        {
            System.out.print("Prime numbers from 1 to " + m + ": ");
            for(int i = 1; i <= m; i++)
            {
                boolean prime = true;
                for(int j = 2; j < i; j++)
                {
                    if(i % j == 0)
                        prime = false;
                }
                if(prime)
                    System.out.println(i + " ");
            }
            {
//5.Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма». Вывести на экран полученную сумму.
                int sum = 0;
                for(int i = 1;i>=1;i++)
                {
                    String numbers = reader.readLine();
                    if(numbers.equals("Sum:"))
                        break;
                    int n = Integer.parseInt(numbers);
                    sum = sum + n;
                }
                System.out.print(sum);
            }
        }
    }
}
